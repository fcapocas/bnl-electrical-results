#
%NewTest
#
SERIAL NUMBER  : BNL-PP2-MLS-005
TEST MADE BY   : qq
LOCATION NAME  : BNL_4MODULEBOX
Run number     : 615-12
TEST_DATE      : 27/12/2019
PASSED         : NO
PROBLEM        : YES
#
%DAQ_INFO
#
#HOST
"localhost"
#VERSION
"3.43"
#DUT
"Star_Test 	#AMAC 0 ComID 0 HCC "
#TIME
"12:09:29"
#
%DCS_INFO
#
#T0	T1
. 	. 	
#VDET	IDET
. 	. 	
#VCC	ICC
. 	. 	
#VDD	IDD
. 	. 	
#TIME_POWERED
. 
#
#
%SCAN_INFO
#
#POINT_TYPE
"TrimDAC (bits)"
#N_POINTS
5
#POINTS
0.00 	8.00 	16.00 	24.00 	31.00 	
#
%Trim
#
#TRIM	CHARGE	TYPE	ALGORITHM
	1.0 	2 	0
#(trim whole module)
#
#	range	target	ntrim	vt50	vt50rms	tr_off	off_rms	tr_step	step_rms
#Ch0
	2	29.0	256	 29.0	 0.23	-31.0	 4.2	 0.51	 0.07
#Ch1
	2	29.0	255	 29.0	 0.23	-32.1	 5.3	 0.53	 0.06
#Ch2
	2	29.0	255	 29.0	 0.24	-29.2	 4.2	 0.51	 0.08
#Ch3
	2	29.0	255	 29.0	 0.25	-31.8	 6.1	 0.55	 0.07
#Ch4
	2	29.0	256	 29.0	 0.22	-35.0	 9.2	 0.47	 0.12
#Ch5
	2	29.0	256	 29.0	 0.26	-42.3	 5.7	 0.30	 0.04
#Ch6
	2	29.0	256	 29.0	 0.23	-39.2	 6.1	 0.37	 0.04
#Ch7
	2	29.0	255	 29.0	 0.25	-38.5	 5.3	 0.34	 0.05
#Ch8
	2	29.0	256	 29.0	 0.22	-38.1	 5.8	 0.38	 0.05
#Ch9
	2	29.0	256	 29.0	 0.24	-37.2	 6.1	 0.38	 0.05
#Ch0
	2	29.0	256	 29.0	 0.24	-31.1	 5.5	 0.51	 0.06
#Ch1
	2	29.0	255	 29.1	 0.23	-32.1	 5.7	 0.51	 0.06
#Ch2
	2	29.0	255	 29.0	 0.24	-30.0	 5.2	 0.50	 0.05
#Ch3
	2	29.0	255	 29.0	 0.22	-31.6	 6.0	 0.51	 0.07
#Ch4
	2	29.0	256	 29.0	 0.23	-34.5	 5.0	 0.41	 0.06
#Ch5
	2	29.0	256	 29.0	 0.26	-37.8	 4.3	 0.34	 0.04
#Ch6
	2	29.0	256	 29.0	 0.25	-37.8	 4.3	 0.37	 0.05
#Ch7
	2	29.0	255	 29.0	 0.27	-37.6	 4.6	 0.33	 0.04
#Ch8
	2	29.0	256	 29.0	 0.22	-38.2	 5.7	 0.38	 0.05
#Ch9
	2	29.0	256	 29.0	 0.24	-37.1	 5.7	 0.38	 0.05
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 0
LAST CHANNEL   : 255
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 256
LAST CHANNEL   : 511
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 512
LAST CHANNEL   : 767
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 768
LAST CHANNEL   : 1023
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 1024
LAST CHANNEL   : 1279
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 1280
LAST CHANNEL   : 1535
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 1536
LAST CHANNEL   : 1791
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 1792
LAST CHANNEL   : 2047
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 2048
LAST CHANNEL   : 2303
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 2304
LAST CHANNEL   : 2559
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 1536
LAST CHANNEL   : 1791
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 1792
LAST CHANNEL   : 2047
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 2048
LAST CHANNEL   : 2303
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 2304
LAST CHANNEL   : 2559
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 2560
LAST CHANNEL   : 2815
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 2816
LAST CHANNEL   : 3071
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 3072
LAST CHANNEL   : 3327
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 3328
LAST CHANNEL   : 3583
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 3584
LAST CHANNEL   : 3839
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 3840
LAST CHANNEL   : 4095
#
%Defect
DEFECT NAME    : TR_NOTRIM
FIRST CHANNEL  : 157
LAST CHANNEL   : 157
#
%Defect
DEFECT NAME    : TR_STEP
FIRST CHANNEL  : 266
LAST CHANNEL   : 266
#
%Defect
DEFECT NAME    : TR_NOTRIM
FIRST CHANNEL  : 298
LAST CHANNEL   : 298
#
%Defect
DEFECT NAME    : TR_NOTRIM
FIRST CHANNEL  : 410
LAST CHANNEL   : 410
#
%Defect
DEFECT NAME    : TR_STEP
FIRST CHANNEL  : 4825
LAST CHANNEL   : 4825
#
%Defect
DEFECT NAME    : TR_OFFSET
FIRST CHANNEL  : 4912
LAST CHANNEL   : 4912
#
%Defect
DEFECT NAME    : TR_STEP
FIRST CHANNEL  : 5040
LAST CHANNEL   : 5040
#
%Defect
DEFECT NAME    : TR_NOTRIM
FIRST CHANNEL  : 5067
LAST CHANNEL   : 5067
#
%Defect
DEFECT NAME    : TR_STEP
FIRST CHANNEL  : 5081
LAST CHANNEL   : 5081
#
%Defect
DEFECT NAME    : TR_STEP
FIRST CHANNEL  : 5209
LAST CHANNEL   : 5209
#
%Defect
DEFECT NAME    : TR_STEP
FIRST CHANNEL  : 5337
LAST CHANNEL   : 5337
#
#31 defects found
%Comment
COMMENT        : L0Ch0 TR 2 step 0.77 (ideal 1.53 error 49.86%)
#
%Comment
COMMENT        : L0Ch1 TR 2 step 0.77 (ideal 1.60 error 51.69%)
#
%Comment
COMMENT        : L0Ch2 TR 2 step 0.76 (ideal 1.52 error 49.75%)
#
%Comment
COMMENT        : L0Ch3 TR 2 step 0.82 (ideal 1.66 error 50.20%)
#
%Comment
COMMENT        : L0Ch4 TR 2 step 0.72 (ideal 1.40 error 48.55%)
#
%Comment
COMMENT        : L0Ch5 TR 2 step 0.52 (ideal 0.71 error 25.83%)
#
%Comment
COMMENT        : L0Ch6 TR 2 step 0.61 (ideal 0.85 error 28.33%)
#
%Comment
COMMENT        : L0Ch7 TR 2 step 0.57 (ideal 0.79 error 27.58%)
#
%Comment
COMMENT        : L0Ch8 TR 2 step 0.61 (ideal 1.14 error 46.47%)
#
%Comment
COMMENT        : L0Ch9 TR 2 step 0.62 (ideal 1.13 error 45.20%)
#
%Comment
COMMENT        : L1Ch0 TR 2 step 0.77 (ideal 1.53 error 49.72%)
#
%Comment
COMMENT        : L1Ch1 TR 2 step 0.76 (ideal 1.52 error 49.97%)
#
%Comment
COMMENT        : L1Ch2 TR 2 step 0.77 (ideal 1.51 error 49.15%)
#
%Comment
COMMENT        : L1Ch3 TR 2 step 0.78 (ideal 1.52 error 48.91%)
#
%Comment
COMMENT        : L1Ch4 TR 2 step 0.66 (ideal 1.23 error 46.47%)
#
%Comment
COMMENT        : L1Ch5 TR 2 step 0.57 (ideal 1.02 error 44.34%)
#
%Comment
COMMENT        : L1Ch6 TR 2 step 0.61 (ideal 0.85 error 28.44%)
#
%Comment
COMMENT        : L1Ch7 TR 2 step 0.56 (ideal 0.99 error 43.41%)
#
%Comment
COMMENT        : L1Ch8 TR 2 step 0.60 (ideal 1.14 error 47.16%)
#
%Comment
COMMENT        : L1Ch9 TR 2 step 0.62 (ideal 1.14 error 45.81%)
#
#
%TEST Rawdata
FILENAME       : /home/qcbox/Desktop/stars/itsdaq-sw/DATA/results/BNL-PP2-MLS-005_tr2_20191227.trim
#
#
%NewTest
#
SERIAL NUMBER  : BNL-PP2-MLS-005
TEST MADE BY   : qq
LOCATION NAME  : BNL_4MODULEBOX
Run number     : 615-17
TEST_DATE      : 27/12/2019
PASSED         : NO
PROBLEM        : NO
#
%DAQ_INFO
#
#HOST
"localhost"
#VERSION
"3.43"
#DUT
"Star_Test 	#AMAC 0 ComID 0 HCC "
#TIME
"12:11:18"
#
%DCS_INFO
#
#T0	T1
. 	. 	
#VDET	IDET
. 	. 	
#VCC	ICC
. 	. 	
#VDD	IDD
. 	. 	
#TIME_POWERED
. 
#
#
%SCAN_INFO
#
#POINT_TYPE
"TrimDAC (bits)"
#N_POINTS
5
#POINTS
0.00 	8.00 	16.00 	24.00 	31.00 	
#
%Trim
#
#TRIM	CHARGE	TYPE	ALGORITHM
	1.0 	4 	0
#(trim whole module)
#
#	range	target	ntrim	vt50	vt50rms	tr_off	off_rms	tr_step	step_rms
#Ch0
	4	34.0	256	 34.1	 0.30	-26.0	 3.6	 0.64	 0.07
#Ch1
	4	34.0	256	 34.0	 0.31	-27.4	 4.7	 0.65	 0.06
#Ch2
	4	34.0	256	 34.0	 0.29	-24.3	 3.4	 0.63	 0.08
#Ch3
	4	34.0	256	 34.0	 0.28	-26.3	 4.9	 0.70	 0.07
#Ch4
	4	34.0	256	 34.0	 0.28	-27.9	 5.9	 0.60	 0.12
#Ch5
	4	34.0	256	 34.0	 0.32	-31.9	 4.0	 0.42	 0.04
#Ch6
	4	34.0	256	 34.0	 0.30	-30.6	 5.3	 0.49	 0.05
#Ch7
	4	34.0	256	 34.0	 0.30	-29.1	 4.5	 0.47	 0.05
#Ch8
	4	34.0	256	 34.0	 0.31	-30.5	 5.0	 0.50	 0.05
#Ch9
	4	34.0	256	 34.0	 0.32	-29.3	 5.0	 0.50	 0.05
#Ch0
	4	34.0	256	 34.0	 0.28	-26.0	 4.9	 0.64	 0.06
#Ch1
	4	34.0	256	 34.0	 0.28	-26.8	 4.5	 0.63	 0.06
#Ch2
	4	34.0	256	 34.0	 0.27	-24.8	 4.3	 0.64	 0.07
#Ch3
	4	34.0	256	 34.0	 0.30	-25.6	 5.1	 0.66	 0.08
#Ch4
	4	34.0	256	 34.0	 0.29	-27.5	 4.1	 0.54	 0.06
#Ch5
	4	34.0	256	 34.0	 0.30	-29.2	 3.5	 0.46	 0.05
#Ch6
	4	34.0	256	 34.0	 0.27	-29.1	 3.8	 0.50	 0.05
#Ch7
	4	34.0	256	 34.0	 0.33	-28.4	 3.8	 0.45	 0.05
#Ch8
	4	34.0	256	 34.0	 0.28	-30.6	 5.0	 0.50	 0.05
#Ch9
	4	34.0	256	 34.0	 0.28	-29.4	 5.1	 0.50	 0.05
#
#No defects found!
#
%TEST Rawdata
FILENAME       : /home/qcbox/Desktop/stars/itsdaq-sw/DATA/results/BNL-PP2-MLS-005_tr4_20191227.trim
#
#
%NewTest
#
SERIAL NUMBER  : BNL-PP2-MLS-005
TEST MADE BY   : qq
LOCATION NAME  : BNL_4MODULEBOX
Run number     : 615-22
TEST_DATE      : 27/12/2019
PASSED         : NO
PROBLEM        : NO
#
%DAQ_INFO
#
#HOST
"localhost"
#VERSION
"3.43"
#DUT
"Star_Test 	#AMAC 0 ComID 0 HCC "
#TIME
"12:13:07"
#
%DCS_INFO
#
#T0	T1
. 	. 	
#VDET	IDET
. 	. 	
#VCC	ICC
. 	. 	
#VDD	IDD
. 	. 	
#TIME_POWERED
. 
#
#
%SCAN_INFO
#
#POINT_TYPE
"TrimDAC (bits)"
#N_POINTS
5
#POINTS
0.00 	8.00 	16.00 	24.00 	31.00 	
#
%Trim
#
#TRIM	CHARGE	TYPE	ALGORITHM
	1.0 	6 	0
#(trim whole module)
#
#	range	target	ntrim	vt50	vt50rms	tr_off	off_rms	tr_step	step_rms
#Ch0
	6	39.0	256	 39.0	 0.32	-22.5	 3.1	 0.77	 0.07
#Ch1
	6	39.0	256	 39.0	 0.33	-23.9	 4.1	 0.77	 0.06
#Ch2
	6	39.0	256	 39.0	 0.34	-21.0	 3.2	 0.76	 0.07
#Ch3
	6	39.0	256	 39.0	 0.34	-23.2	 4.6	 0.82	 0.07
#Ch4
	6	39.0	256	 39.0	 0.33	-24.2	 5.3	 0.72	 0.13
#Ch5
	6	39.0	256	 39.0	 0.40	-27.1	 3.7	 0.52	 0.04
#Ch6
	6	39.0	256	 39.0	 0.32	-26.2	 4.4	 0.61	 0.05
#Ch7
	6	39.0	256	 39.0	 0.37	-25.2	 4.4	 0.57	 0.04
#Ch8
	6	39.0	256	 39.0	 0.32	-26.1	 4.7	 0.61	 0.04
#Ch9
	6	39.0	256	 39.0	 0.35	-24.9	 4.5	 0.62	 0.05
#Ch0
	6	39.0	256	 39.0	 0.35	-22.5	 4.3	 0.77	 0.06
#Ch1
	6	39.0	256	 39.0	 0.35	-23.2	 4.2	 0.76	 0.06
#Ch2
	6	39.0	256	 39.0	 0.35	-21.5	 4.1	 0.77	 0.06
#Ch3
	6	39.0	256	 39.0	 0.36	-22.7	 4.8	 0.78	 0.07
#Ch4
	6	39.0	256	 39.0	 0.34	-23.7	 3.8	 0.66	 0.06
#Ch5
	6	39.0	256	 39.0	 0.40	-24.7	 3.7	 0.57	 0.04
#Ch6
	6	39.0	256	 39.0	 0.37	-25.3	 3.3	 0.61	 0.05
#Ch7
	6	39.0	256	 39.0	 0.33	-24.3	 3.7	 0.56	 0.04
#Ch8
	6	39.0	256	 39.0	 0.35	-26.4	 4.6	 0.60	 0.04
#Ch9
	6	39.0	256	 39.0	 0.34	-25.2	 4.5	 0.62	 0.06
#
#No defects found!
#
%TEST Rawdata
FILENAME       : /home/qcbox/Desktop/stars/itsdaq-sw/DATA/results/BNL-PP2-MLS-005_tr6_20191227.trim
#
#
%NewTest
#
SERIAL NUMBER  : BNL-PP2-MLS-005
TEST MADE BY   : qq
LOCATION NAME  : BNL_4MODULEBOX
Run number     : 615-27
TEST_DATE      : 27/12/2019
PASSED         : NO
PROBLEM        : NO
#
%DAQ_INFO
#
#HOST
"localhost"
#VERSION
"3.43"
#DUT
"Star_Test 	#AMAC 0 ComID 0 HCC "
#TIME
"12:15:01"
#
%DCS_INFO
#
#T0	T1
. 	. 	
#VDET	IDET
. 	. 	
#VCC	ICC
. 	. 	
#VDD	IDD
. 	. 	
#TIME_POWERED
. 
#
#
%SCAN_INFO
#
#POINT_TYPE
"TrimDAC (bits)"
#N_POINTS
5
#POINTS
0.00 	8.00 	16.00 	24.00 	31.00 	
#
%Trim
#
#TRIM	CHARGE	TYPE	ALGORITHM
	1.0 	8 	0
#(trim whole module)
#
#	range	target	ntrim	vt50	vt50rms	tr_off	off_rms	tr_step	step_rms
#Ch0
	8	44.0	256	 44.0	 0.39	-18.5	 2.7	 0.96	 0.09
#Ch1
	8	44.0	256	 44.0	 0.39	-19.6	 3.2	 0.97	 0.08
#Ch2
	8	44.0	256	 43.9	 0.38	-17.4	 2.6	 0.94	 0.08
#Ch3
	8	44.0	256	 44.0	 0.43	-19.3	 3.8	 1.02	 0.07
#Ch4
	8	44.0	256	 44.0	 0.35	-20.0	 3.6	 0.88	 0.13
#Ch5
	8	44.0	256	 44.0	 0.43	-21.2	 2.5	 0.68	 0.05
#Ch6
	8	44.0	256	 44.0	 0.42	-20.3	 3.1	 0.80	 0.07
#Ch7
	8	44.0	256	 44.0	 0.44	-19.7	 3.0	 0.75	 0.07
#Ch8
	8	44.0	256	 44.0	 0.41	-20.4	 3.1	 0.79	 0.07
#Ch9
	8	44.0	256	 44.0	 0.38	-19.8	 3.2	 0.79	 0.07
#Ch0
	8	44.0	256	 43.9	 0.40	-18.6	 3.5	 0.96	 0.08
#Ch1
	8	44.0	256	 44.0	 0.40	-19.2	 3.2	 0.95	 0.07
#Ch2
	8	44.0	256	 43.9	 0.39	-17.9	 3.1	 0.95	 0.07
#Ch3
	8	44.0	256	 44.0	 0.41	-18.5	 3.5	 0.98	 0.09
#Ch4
	8	44.0	256	 43.9	 0.42	-19.2	 2.8	 0.83	 0.09
#Ch5
	8	44.0	256	 44.0	 0.39	-19.6	 2.9	 0.73	 0.06
#Ch6
	8	44.0	256	 44.1	 0.44	-19.7	 2.4	 0.79	 0.06
#Ch7
	8	44.0	256	 44.1	 0.43	-19.2	 2.5	 0.73	 0.06
#Ch8
	8	44.0	256	 44.0	 0.39	-20.7	 3.3	 0.79	 0.07
#Ch9
	8	44.0	256	 44.0	 0.40	-19.8	 3.2	 0.80	 0.07
#
#No defects found!
#
%TEST Rawdata
FILENAME       : /home/qcbox/Desktop/stars/itsdaq-sw/DATA/results/BNL-PP2-MLS-005_tr8_20191227.trim
#
#
%NewTest
#
SERIAL NUMBER  : BNL-PP2-MLS-005
TEST MADE BY   : qq
LOCATION NAME  : BNL_4MODULEBOX
Run number     : 615-12
TEST_DATE      : 27/12/2019
PASSED         : NO
PROBLEM        : YES
#
%DAQ_INFO
#
#HOST
"localhost"
#VERSION
"3.43"
#DUT
"Star_Test 	#AMAC 0 ComID 0 HCC "
#TIME
"12:09:29"
#
%DCS_INFO
#
#T0	T1
. 	. 	
#VDET	IDET
. 	. 	
#VCC	ICC
. 	. 	
#VDD	IDD
. 	. 	
#TIME_POWERED
. 
#
#
%Trim
#
#TRIM	CHARGE	TYPE	ALGORITHM
	1.0 	-1 	0
#(trim whole module)
#
#	range	target	ntrim	vt50	vt50rms	tr_off	off_rms	tr_step	step_rms
#Ch0
	2	29.0	256	 29.0	 0.23	-31.0	 4.2	 0.51	 0.07
#Ch1
	2	29.0	255	 29.0	 0.23	-32.1	 5.3	 0.53	 0.06
#Ch2
	4	29.0	256	 29.0	 0.29	-24.3	 3.4	 0.63	 0.08
#Ch3
	2	29.0	255	 29.0	 0.25	-31.8	 6.1	 0.55	 0.07
#Ch4
	2	29.0	256	 29.0	 0.22	-35.0	 9.2	 0.47	 0.12
#Ch5
	2	29.0	256	 29.0	 0.26	-42.3	 5.7	 0.30	 0.04
#Ch6
	2	29.0	256	 29.0	 0.23	-39.2	 6.1	 0.37	 0.04
#Ch7
	4	29.0	256	 29.0	 0.26	-29.1	 4.5	 0.47	 0.05
#Ch8
	2	29.0	256	 29.0	 0.22	-38.1	 5.8	 0.38	 0.05
#Ch9
	2	29.0	256	 29.0	 0.24	-37.2	 6.1	 0.38	 0.05
#Ch0
	2	29.0	256	 29.0	 0.24	-31.1	 5.5	 0.51	 0.06
#Ch1
	2	29.0	255	 29.1	 0.23	-32.1	 5.7	 0.51	 0.06
#Ch2
	4	29.0	256	 29.0	 0.26	-24.8	 4.3	 0.64	 0.07
#Ch3
	2	29.0	255	 29.0	 0.22	-31.6	 6.0	 0.51	 0.07
#Ch4
	2	29.0	256	 29.0	 0.23	-34.5	 5.0	 0.41	 0.06
#Ch5
	2	29.0	256	 29.0	 0.26	-37.8	 4.3	 0.34	 0.04
#Ch6
	2	29.0	256	 29.0	 0.25	-37.8	 4.3	 0.37	 0.05
#Ch7
	4	29.0	256	 29.0	 0.27	-28.4	 3.8	 0.45	 0.05
#Ch8
	2	29.0	256	 29.0	 0.22	-38.2	 5.7	 0.38	 0.05
#Ch9
	2	29.0	256	 29.0	 0.24	-37.1	 5.7	 0.38	 0.05
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 0
LAST CHANNEL   : 255
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 256
LAST CHANNEL   : 511
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 768
LAST CHANNEL   : 1023
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 1024
LAST CHANNEL   : 1279
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 1280
LAST CHANNEL   : 1535
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 1536
LAST CHANNEL   : 1791
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 2048
LAST CHANNEL   : 2303
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 2304
LAST CHANNEL   : 2559
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 1536
LAST CHANNEL   : 1791
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 1792
LAST CHANNEL   : 2047
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 2304
LAST CHANNEL   : 2559
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 2560
LAST CHANNEL   : 2815
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 2816
LAST CHANNEL   : 3071
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 3072
LAST CHANNEL   : 3327
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 3584
LAST CHANNEL   : 3839
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 3840
LAST CHANNEL   : 4095
#
%Defect
DEFECT NAME    : TR_NOTRIM
FIRST CHANNEL  : 157
LAST CHANNEL   : 157
#
%Defect
DEFECT NAME    : TR_NOTRIM
FIRST CHANNEL  : 410
LAST CHANNEL   : 410
#
%Defect
DEFECT NAME    : TR_STEP
FIRST CHANNEL  : 4825
LAST CHANNEL   : 4825
#
%Defect
DEFECT NAME    : TR_OFFSET
FIRST CHANNEL  : 4912
LAST CHANNEL   : 4912
#
%Defect
DEFECT NAME    : TR_STEP
FIRST CHANNEL  : 5209
LAST CHANNEL   : 5209
#
%Defect
DEFECT NAME    : TR_STEP
FIRST CHANNEL  : 5337
LAST CHANNEL   : 5337
#
#22 defects found
%Comment
COMMENT        : L0Ch0 TR 2 step 0.77 (ideal 1.53 error 49.86%)
#
%Comment
COMMENT        : L0Ch1 TR 2 step 0.77 (ideal 1.60 error 51.69%)
#
%Comment
COMMENT        : L0Ch3 TR 2 step 0.82 (ideal 1.66 error 50.20%)
#
%Comment
COMMENT        : L0Ch4 TR 2 step 0.72 (ideal 1.40 error 48.55%)
#
%Comment
COMMENT        : L0Ch5 TR 2 step 0.52 (ideal 0.71 error 25.83%)
#
%Comment
COMMENT        : L0Ch6 TR 2 step 0.61 (ideal 0.85 error 28.33%)
#
%Comment
COMMENT        : L0Ch8 TR 2 step 0.61 (ideal 1.14 error 46.47%)
#
%Comment
COMMENT        : L0Ch9 TR 2 step 0.62 (ideal 1.13 error 45.20%)
#
%Comment
COMMENT        : L1Ch0 TR 2 step 0.77 (ideal 1.53 error 49.72%)
#
%Comment
COMMENT        : L1Ch1 TR 2 step 0.76 (ideal 1.52 error 49.97%)
#
%Comment
COMMENT        : L1Ch3 TR 2 step 0.78 (ideal 1.52 error 48.91%)
#
%Comment
COMMENT        : L1Ch4 TR 2 step 0.66 (ideal 1.23 error 46.47%)
#
%Comment
COMMENT        : L1Ch5 TR 2 step 0.57 (ideal 1.02 error 44.34%)
#
%Comment
COMMENT        : L1Ch6 TR 2 step 0.61 (ideal 0.85 error 28.44%)
#
%Comment
COMMENT        : L1Ch8 TR 2 step 0.60 (ideal 1.14 error 47.16%)
#
%Comment
COMMENT        : L1Ch9 TR 2 step 0.62 (ideal 1.14 error 45.81%)
#
#
%TEST Rawdata
FILENAME       : /home/qcbox/Desktop/stars/itsdaq-sw/DATA/results/BNL-PP2-MLS-005_tr-1_20191227.trim
#
#
%NewTest
#
SERIAL NUMBER  : BNL-PP2-MLS-005
TEST MADE BY   : qq
LOCATION NAME  : BNL_4MODULEBOX
Run number     : 617-2
TEST_DATE      : 27/12/2019
PASSED         : YES
PROBLEM        : NO
#
%DAQ_INFO
#
#HOST
"localhost"
#VERSION
"3.43"
#DUT
"Star_Test 	#AMAC 0 ComID 0 HCC "
#TIME
"12:20:24"
#
%DCS_INFO
#
#T0	T1
. 	. 	
#VDET	IDET
. 	. 	
#VCC	ICC
. 	. 	
#VDD	IDD
. 	. 	
#TIME_POWERED
. 
#
%StrobeDelay
#
#DELAY
#  M00  S01  S02  S03  S04  S05  S06  S07  S08  E09
14 17 16 16 17 17 15 15 19 16 -1 -1 -1 -1 -1 -1 
#
#  M00  S01  S02  S03  S04  S05  S06  S07  S08  E09
14 17 16 16 17 17 16 15 19 16 -1 -1 -1 -1 -1 -1 
#
#No defects found!
%Comment
COMMENT        : Strobe Delay Fraction 0.57
#
#
%NewTest
#
SERIAL NUMBER  : BNL-PP2-MLS-005
TEST MADE BY   : qq
LOCATION NAME  : BNL_4MODULEBOX
Run number     : 618-7
TEST_DATE      : 27/12/2019
PASSED         : NO
PROBLEM        : YES
#
%DAQ_INFO
#
#HOST
"localhost"
#VERSION
"3.43"
#DUT
"Star_Test 	#AMAC 0 ComID 0 HCC "
#TIME
"12:31:29"
#
%DCS_INFO
#
#T0	T1
. 	. 	
#VDET	IDET
. 	. 	
#VCC	ICC
. 	. 	
#VDD	IDD
. 	. 	
#TIME_POWERED
. 
#
#
%SCAN_INFO
#
#POINT_TYPE
"TrimDAC (bits)"
#N_POINTS
5
#POINTS
0.00 	8.00 	16.00 	24.00 	31.00 	
#
%Trim
#
#TRIM	CHARGE	TYPE	ALGORITHM
	1.0 	2 	0
#(trim whole module)
#
#	range	target	ntrim	vt50	vt50rms	tr_off	off_rms	tr_step	step_rms
#Ch0
	2	26.0	256	 26.0	 0.25	-23.4	 6.5	 0.61	 0.06
#Ch1
	2	26.0	254	 26.0	 0.25	-24.9	 7.3	 0.63	 0.06
#Ch2
	2	26.0	255	 26.0	 0.26	-19.3	 6.0	 0.63	 0.06
#Ch3
	2	26.0	255	 26.0	 0.25	-22.6	 7.6	 0.64	 0.07
#Ch4
	2	26.0	256	 26.0	 0.24	-21.7	 7.0	 0.60	 0.06
#Ch5
	2	26.0	245	 26.0	 0.23	-19.9	 6.3	 0.55	 0.05
#Ch6
	2	26.0	255	 26.0	 0.24	-20.4	 7.8	 0.61	 0.06
#Ch7
	2	26.0	254	 26.0	 0.24	-22.1	 7.6	 0.55	 0.05
#Ch8
	2	26.0	255	 26.0	 0.23	-23.5	 7.2	 0.59	 0.05
#Ch9
	2	26.0	256	 26.0	 0.25	-20.8	 8.1	 0.58	 0.06
#Ch0
	2	26.0	256	 26.0	 0.25	-24.0	 8.6	 0.61	 0.06
#Ch1
	2	26.0	254	 26.0	 0.23	-24.1	 7.3	 0.62	 0.06
#Ch2
	2	26.0	255	 26.0	 0.24	-20.7	 7.1	 0.63	 0.06
#Ch3
	2	26.0	255	 26.0	 0.26	-21.2	 8.2	 0.65	 0.06
#Ch4
	2	26.0	256	 26.0	 0.22	-22.6	 8.0	 0.58	 0.07
#Ch5
	2	26.0	245	 26.0	 0.23	-19.9	 6.6	 0.56	 0.05
#Ch6
	2	26.0	255	 26.0	 0.23	-20.4	 6.8	 0.60	 0.06
#Ch7
	2	26.0	254	 26.0	 0.21	-21.3	 6.6	 0.55	 0.04
#Ch8
	2	26.0	255	 26.0	 0.24	-24.4	 7.8	 0.58	 0.06
#Ch9
	2	26.0	256	 26.0	 0.24	-21.0	 7.7	 0.59	 0.06
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 0
LAST CHANNEL   : 255
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 256
LAST CHANNEL   : 511
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 512
LAST CHANNEL   : 767
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 768
LAST CHANNEL   : 1023
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 1024
LAST CHANNEL   : 1279
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 1280
LAST CHANNEL   : 1535
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 1536
LAST CHANNEL   : 1791
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 1792
LAST CHANNEL   : 2047
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 2048
LAST CHANNEL   : 2303
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 2304
LAST CHANNEL   : 2559
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 1536
LAST CHANNEL   : 1791
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 1792
LAST CHANNEL   : 2047
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 2048
LAST CHANNEL   : 2303
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 2304
LAST CHANNEL   : 2559
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 2560
LAST CHANNEL   : 2815
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 2816
LAST CHANNEL   : 3071
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 3072
LAST CHANNEL   : 3327
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 3328
LAST CHANNEL   : 3583
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 3584
LAST CHANNEL   : 3839
#
%Defect
DEFECT NAME    : TR_RANGE
FIRST CHANNEL  : 3840
LAST CHANNEL   : 4095
#
%Defect
DEFECT NAME    : TR_NOTRIM
FIRST CHANNEL  : 157
LAST CHANNEL   : 157
#
%Defect
DEFECT NAME    : TR_NOTRIM
FIRST CHANNEL  : 410
LAST CHANNEL   : 410
#
%Defect
DEFECT NAME    : TR_NOTRIM
FIRST CHANNEL  : 659
LAST CHANNEL   : 659
#
%Defect
DEFECT NAME    : TR_NOTRIM
FIRST CHANNEL  : 663
LAST CHANNEL   : 663
#
%Defect
DEFECT NAME    : TR_NOTRIM
FIRST CHANNEL  : 667
LAST CHANNEL   : 667
#
%Defect
DEFECT NAME    : TR_NOTRIM
FIRST CHANNEL  : 680
LAST CHANNEL   : 680
#
%Defect
DEFECT NAME    : TR_NOTRIM
FIRST CHANNEL  : 696
LAST CHANNEL   : 696
#
%Defect
DEFECT NAME    : TR_NOTRIM
FIRST CHANNEL  : 700
LAST CHANNEL   : 700
#
%Defect
DEFECT NAME    : TR_NOTRIM
FIRST CHANNEL  : 720
LAST CHANNEL   : 720
#
%Defect
DEFECT NAME    : TR_NOTRIM
FIRST CHANNEL  : 762
LAST CHANNEL   : 762
#
%Defect
DEFECT NAME    : TR_NOTRIM
FIRST CHANNEL  : 1142
LAST CHANNEL   : 1142
#
%Defect
DEFECT NAME    : TR_NOTRIM
FIRST CHANNEL  : 4342
LAST CHANNEL   : 4342
#
%Defect
DEFECT NAME    : TR_NOTRIM
FIRST CHANNEL  : 4373
LAST CHANNEL   : 4373
#
%Defect
DEFECT NAME    : TR_NOTRIM
FIRST CHANNEL  : 4776
LAST CHANNEL   : 4776
#
%Defect
DEFECT NAME    : TR_NOTRIM
FIRST CHANNEL  : 4795
LAST CHANNEL   : 4795
#
%Defect
DEFECT NAME    : TR_NOTRIM
FIRST CHANNEL  : 4846
LAST CHANNEL   : 4846
#
%Defect
DEFECT NAME    : TR_NOTRIM
FIRST CHANNEL  : 4941
LAST CHANNEL   : 4941
#
%Defect
DEFECT NAME    : TR_NOTRIM
FIRST CHANNEL  : 5067
LAST CHANNEL   : 5067
#
%Defect
DEFECT NAME    : TR_NOTRIM
FIRST CHANNEL  : 5090
LAST CHANNEL   : 5090
#
#39 defects found
%Comment
COMMENT        : L0Ch0 TR 2 step 0.90 (ideal 1.82 error 50.37%)
#
%Comment
COMMENT        : L0Ch1 TR 2 step 0.91 (ideal 1.89 error 51.76%)
#
%Comment
COMMENT        : L0Ch2 TR 2 step 0.90 (ideal 1.88 error 51.89%)
#
%Comment
COMMENT        : L0Ch3 TR 2 step 0.94 (ideal 1.92 error 51.01%)
#
%Comment
COMMENT        : L0Ch4 TR 2 step 0.88 (ideal 1.80 error 51.30%)
#
%Comment
COMMENT        : L0Ch5 TR 2 step 0.79 (ideal 1.66 error 52.04%)
#
%Comment
COMMENT        : L0Ch6 TR 2 step 0.89 (ideal 1.83 error 51.71%)
#
%Comment
COMMENT        : L0Ch7 TR 2 step 0.81 (ideal 1.66 error 51.06%)
#
%Comment
COMMENT        : L0Ch8 TR 2 step 0.85 (ideal 1.76 error 51.97%)
#
%Comment
COMMENT        : L0Ch9 TR 2 step 0.84 (ideal 1.74 error 51.61%)
#
%Comment
COMMENT        : L1Ch0 TR 2 step 0.90 (ideal 1.82 error 50.37%)
#
%Comment
COMMENT        : L1Ch1 TR 2 step 0.90 (ideal 1.85 error 51.57%)
#
%Comment
COMMENT        : L1Ch2 TR 2 step 0.90 (ideal 1.88 error 51.84%)
#
%Comment
COMMENT        : L1Ch3 TR 2 step 0.94 (ideal 1.94 error 51.48%)
#
%Comment
COMMENT        : L1Ch4 TR 2 step 0.86 (ideal 1.75 error 51.01%)
#
%Comment
COMMENT        : L1Ch5 TR 2 step 0.81 (ideal 1.67 error 51.69%)
#
%Comment
COMMENT        : L1Ch6 TR 2 step 0.88 (ideal 1.81 error 51.59%)
#
%Comment
COMMENT        : L1Ch7 TR 2 step 0.81 (ideal 1.66 error 51.05%)
#
%Comment
COMMENT        : L1Ch8 TR 2 step 0.84 (ideal 1.74 error 51.99%)
#
%Comment
COMMENT        : L1Ch9 TR 2 step 0.85 (ideal 1.76 error 51.84%)
#
%Comment
COMMENT        : FAIL due to 19 bad strips (more than 15)
#
#
%TEST Rawdata
FILENAME       : /home/qcbox/Desktop/stars/itsdaq-sw/DATA/results/BNL-PP2-MLS-005_tr2_20191227.trim
#
#
%NewTest
#
SERIAL NUMBER  : BNL-PP2-MLS-005
TEST MADE BY   : qq
LOCATION NAME  : BNL_4MODULEBOX
Run number     : 618-12
TEST_DATE      : 27/12/2019
PASSED         : NO
PROBLEM        : YES
#
%DAQ_INFO
#
#HOST
"localhost"
#VERSION
"3.43"
#DUT
"Star_Test 	#AMAC 0 ComID 0 HCC "
#TIME
"12:34:16"
#
%DCS_INFO
#
#T0	T1
. 	. 	
#VDET	IDET
. 	. 	
#VCC	ICC
. 	. 	
#VDD	IDD
. 	. 	
#TIME_POWERED
. 
#
#
%SCAN_INFO
#
#POINT_TYPE
"TrimDAC (bits)"
#N_POINTS
5
#POINTS
0.00 	8.00 	16.00 	24.00 	31.00 	
#
%Trim
#
#TRIM	CHARGE	TYPE	ALGORITHM
	1.0 	4 	0
#(trim whole module)
#
#	range	target	ntrim	vt50	vt50rms	tr_off	off_rms	tr_step	step_rms
#Ch0
	4	28.0	256	 28.0	 0.27	-19.9	 5.1	 0.74	 0.06
#Ch1
	4	28.0	255	 28.0	 0.27	-21.2	 5.9	 0.76	 0.06
#Ch2
	4	28.0	256	 28.0	 0.26	-17.0	 5.4	 0.75	 0.07
#Ch3
	4	28.0	255	 28.0	 0.26	-19.3	 6.1	 0.78	 0.07
#Ch4
	4	28.0	256	 28.0	 0.26	-18.6	 5.7	 0.72	 0.06
#Ch5
	4	28.0	256	 28.0	 0.29	-16.4	 6.1	 0.67	 0.06
#Ch6
	4	28.0	256	 28.0	 0.28	-17.7	 6.4	 0.73	 0.07
#Ch7
	4	28.0	256	 28.0	 0.25	-19.0	 6.3	 0.67	 0.06
#Ch8
	4	28.0	256	 28.0	 0.28	-20.4	 6.1	 0.70	 0.06
#Ch9
	4	28.0	256	 28.0	 0.27	-17.9	 6.8	 0.70	 0.06
#Ch0
	4	28.0	256	 28.0	 0.28	-20.3	 6.7	 0.74	 0.06
#Ch1
	4	28.0	255	 28.1	 0.25	-20.6	 6.0	 0.75	 0.07
#Ch2
	4	28.0	256	 28.0	 0.29	-17.8	 5.8	 0.75	 0.07
#Ch3
	4	28.0	255	 28.0	 0.29	-18.2	 6.6	 0.78	 0.07
#Ch4
	4	28.0	256	 28.0	 0.27	-19.3	 6.3	 0.71	 0.06
#Ch5
	4	28.0	256	 28.0	 0.27	-16.8	 5.8	 0.67	 0.06
#Ch6
	4	28.0	256	 28.0	 0.27	-17.5	 5.6	 0.73	 0.06
#Ch7
	4	28.0	256	 28.0	 0.26	-18.0	 5.6	 0.67	 0.05
#Ch8
	4	28.0	256	 28.0	 0.27	-21.0	 6.3	 0.70	 0.06
#Ch9
	4	28.0	256	 28.0	 0.28	-18.0	 6.3	 0.71	 0.06
#
#No defects found!
#
%TEST Rawdata
FILENAME       : /home/qcbox/Desktop/stars/itsdaq-sw/DATA/results/BNL-PP2-MLS-005_tr4_20191227.trim
#
#
%NewTest
#
SERIAL NUMBER  : BNL-PP2-MLS-005
TEST MADE BY   : qq
LOCATION NAME  : BNL_4MODULEBOX
Run number     : 618-17
TEST_DATE      : 27/12/2019
PASSED         : NO
PROBLEM        : NO
#
%DAQ_INFO
#
#HOST
"localhost"
#VERSION
"3.43"
#DUT
"Star_Test 	#AMAC 0 ComID 0 HCC "
#TIME
"12:36:18"
#
%DCS_INFO
#
#T0	T1
. 	. 	
#VDET	IDET
. 	. 	
#VCC	ICC
. 	. 	
#VDD	IDD
. 	. 	
#TIME_POWERED
. 
#
#
%SCAN_INFO
#
#POINT_TYPE
"TrimDAC (bits)"
#N_POINTS
5
#POINTS
0.00 	8.00 	16.00 	24.00 	31.00 	
#
%Trim
#
#TRIM	CHARGE	TYPE	ALGORITHM
	1.0 	6 	0
#(trim whole module)
#
#	range	target	ntrim	vt50	vt50rms	tr_off	off_rms	tr_step	step_rms
#Ch0
	6	32.0	256	 32.0	 0.33	-16.8	 4.2	 0.90	 0.07
#Ch1
	6	32.0	256	 32.0	 0.34	-18.4	 5.2	 0.91	 0.07
#Ch2
	6	32.0	256	 32.0	 0.33	-14.5	 4.3	 0.90	 0.07
#Ch3
	6	32.0	256	 32.0	 0.34	-16.5	 5.2	 0.94	 0.07
#Ch4
	6	32.0	256	 31.9	 0.32	-15.8	 4.6	 0.88	 0.06
#Ch5
	6	32.0	256	 32.0	 0.31	-14.2	 4.9	 0.79	 0.06
#Ch6
	6	32.0	256	 32.0	 0.33	-15.1	 5.2	 0.89	 0.07
#Ch7
	6	32.0	256	 32.1	 0.32	-16.1	 5.1	 0.81	 0.06
#Ch8
	6	32.0	256	 32.0	 0.33	-17.6	 5.2	 0.85	 0.06
#Ch9
	6	32.0	256	 32.0	 0.32	-15.4	 5.6	 0.84	 0.07
#Ch0
	6	32.0	256	 32.0	 0.34	-17.1	 5.4	 0.90	 0.07
#Ch1
	6	32.0	256	 32.0	 0.34	-17.7	 4.9	 0.90	 0.07
#Ch2
	6	32.0	256	 32.0	 0.32	-15.2	 4.8	 0.90	 0.07
#Ch3
	6	32.0	256	 32.0	 0.36	-15.6	 5.5	 0.94	 0.07
#Ch4
	6	32.0	256	 32.1	 0.31	-16.4	 5.2	 0.86	 0.07
#Ch5
	6	32.0	256	 32.0	 0.32	-14.4	 4.7	 0.81	 0.06
#Ch6
	6	32.0	256	 32.0	 0.31	-14.9	 4.5	 0.88	 0.06
#Ch7
	6	32.0	256	 32.0	 0.33	-15.3	 4.6	 0.81	 0.05
#Ch8
	6	32.0	256	 32.0	 0.32	-18.0	 5.2	 0.84	 0.06
#Ch9
	6	32.0	256	 32.0	 0.33	-15.5	 5.3	 0.85	 0.06
#
#No defects found!
#
%TEST Rawdata
FILENAME       : /home/qcbox/Desktop/stars/itsdaq-sw/DATA/results/BNL-PP2-MLS-005_tr6_20191227.trim
#
#
%NewTest
#
SERIAL NUMBER  : BNL-PP2-MLS-005
TEST MADE BY   : qq
LOCATION NAME  : BNL_4MODULEBOX
Run number     : 618-22
TEST_DATE      : 27/12/2019
PASSED         : NO
PROBLEM        : NO
#
%DAQ_INFO
#
#HOST
"localhost"
#VERSION
"3.43"
#DUT
"Star_Test 	#AMAC 0 ComID 0 HCC "
#TIME
"12:38:27"
#
%DCS_INFO
#
#T0	T1
. 	. 	
#VDET	IDET
. 	. 	
#VCC	ICC
. 	. 	
#VDD	IDD
. 	. 	
#TIME_POWERED
. 
#
#
%SCAN_INFO
#
#POINT_TYPE
"TrimDAC (bits)"
#N_POINTS
5
#POINTS
0.00 	8.00 	16.00 	24.00 	31.00 	
#
%Trim
#
#TRIM	CHARGE	TYPE	ALGORITHM
	1.0 	8 	0
#(trim whole module)
#
#	range	target	ntrim	vt50	vt50rms	tr_off	off_rms	tr_step	step_rms
#Ch0
	8	37.0	256	 37.0	 0.36	-14.8	 3.5	 1.05	 0.07
#Ch1
	8	37.0	256	 37.0	 0.35	-16.2	 4.1	 1.06	 0.07
#Ch2
	8	37.0	256	 37.0	 0.37	-12.9	 3.7	 1.05	 0.08
#Ch3
	8	37.0	256	 37.0	 0.40	-14.6	 4.5	 1.10	 0.08
#Ch4
	8	37.0	256	 37.0	 0.38	-14.1	 3.9	 1.02	 0.06
#Ch5
	8	37.0	256	 37.0	 0.35	-12.4	 4.1	 0.94	 0.06
#Ch6
	8	37.0	256	 37.0	 0.38	-13.4	 4.5	 1.03	 0.07
#Ch7
	8	37.0	256	 37.0	 0.36	-14.2	 4.3	 0.95	 0.06
#Ch8
	8	37.0	256	 37.0	 0.35	-15.4	 4.4	 0.99	 0.07
#Ch9
	8	37.0	256	 37.0	 0.39	-13.5	 4.7	 0.99	 0.07
#Ch0
	8	37.0	256	 37.0	 0.38	-15.1	 4.6	 1.05	 0.07
#Ch1
	8	37.0	256	 37.0	 0.38	-15.6	 4.1	 1.05	 0.07
#Ch2
	8	37.0	256	 37.0	 0.41	-13.5	 4.1	 1.05	 0.07
#Ch3
	8	37.0	256	 37.0	 0.39	-13.9	 4.7	 1.10	 0.07
#Ch4
	8	37.0	256	 37.0	 0.40	-14.5	 4.4	 1.00	 0.08
#Ch5
	8	37.0	256	 37.0	 0.36	-12.7	 3.9	 0.95	 0.06
#Ch6
	8	37.0	256	 37.0	 0.39	-13.2	 3.9	 1.03	 0.06
#Ch7
	8	37.0	256	 36.9	 0.33	-13.6	 4.0	 0.95	 0.06
#Ch8
	8	37.0	256	 37.0	 0.39	-15.8	 4.4	 0.98	 0.07
#Ch9
	8	37.0	256	 37.0	 0.38	-13.7	 4.4	 0.99	 0.07
#
#No defects found!
#
%TEST Rawdata
FILENAME       : /home/qcbox/Desktop/stars/itsdaq-sw/DATA/results/BNL-PP2-MLS-005_tr8_20191227.trim
#
#
%NewTest
#
SERIAL NUMBER  : BNL-PP2-MLS-005
TEST MADE BY   : qq
LOCATION NAME  : BNL_4MODULEBOX
Run number     : 618-7
TEST_DATE      : 27/12/2019
PASSED         : NO
PROBLEM        : NO
#
%DAQ_INFO
#
#HOST
"localhost"
#VERSION
"3.43"
#DUT
"Star_Test 	#AMAC 0 ComID 0 HCC "
#TIME
"12:31:29"
#
%DCS_INFO
#
#T0	T1
. 	. 	
#VDET	IDET
. 	. 	
#VCC	ICC
. 	. 	
#VDD	IDD
. 	. 	
#TIME_POWERED
. 
#
#
%Trim
#
#TRIM	CHARGE	TYPE	ALGORITHM
	1.0 	-1 	0
#(trim whole module)
#
#	range	target	ntrim	vt50	vt50rms	tr_off	off_rms	tr_step	step_rms
#Ch0
	6	39.0	256	 39.0	 0.36	-16.8	 4.2	 0.90	 0.07
#Ch1
	6	39.0	256	 39.0	 0.37	-18.4	 5.2	 0.91	 0.07
#Ch2
	8	39.0	256	 39.0	 0.40	-12.9	 3.7	 1.05	 0.08
#Ch3
	6	39.0	256	 39.0	 0.34	-16.5	 5.2	 0.94	 0.07
#Ch4
	6	39.0	256	 39.0	 0.34	-15.8	 4.6	 0.88	 0.06
#Ch5
	8	39.0	256	 39.0	 0.39	-12.4	 4.1	 0.94	 0.06
#Ch6
	8	39.0	256	 39.0	 0.39	-13.4	 4.5	 1.03	 0.07
#Ch7
	8	39.0	255	 39.0	 0.39	-14.2	 4.3	 0.95	 0.06
#Ch8
	8	39.0	256	 39.0	 0.39	-15.4	 4.4	 0.99	 0.07
#Ch9
	8	39.0	256	 39.1	 0.35	-13.5	 4.7	 0.99	 0.07
#Ch0
	6	39.0	256	 39.0	 0.37	-17.1	 5.4	 0.90	 0.07
#Ch1
	6	39.0	256	 39.0	 0.36	-17.7	 4.9	 0.90	 0.07
#Ch2
	8	39.0	256	 39.1	 0.37	-13.5	 4.1	 1.05	 0.07
#Ch3
	6	39.0	256	 39.0	 0.36	-15.6	 5.5	 0.94	 0.07
#Ch4
	6	39.0	256	 39.0	 0.34	-16.4	 5.2	 0.86	 0.07
#Ch5
	8	39.0	256	 39.1	 0.37	-12.7	 3.9	 0.95	 0.06
#Ch6
	8	39.0	256	 39.0	 0.39	-13.2	 3.9	 1.03	 0.06
#Ch7
	8	39.0	255	 39.0	 0.40	-13.6	 4.0	 0.95	 0.06
#Ch8
	8	39.0	256	 39.0	 0.36	-15.8	 4.4	 0.98	 0.07
#Ch9
	8	39.0	256	 39.0	 0.38	-13.7	 4.4	 0.99	 0.07
#
#No defects found!
#
%TEST Rawdata
FILENAME       : /home/qcbox/Desktop/stars/itsdaq-sw/DATA/results/BNL-PP2-MLS-005_tr-1_20191227.trim
#
#
%NewTest
#
SERIAL NUMBER  : BNL-PP2-MLS-005
TEST MADE BY   : qq
LOCATION NAME  : BNL_4MODULEBOX
Run number     : 620-2
TEST_DATE      : 27/12/2019
PASSED         : YES
PROBLEM        : NO
#
%DAQ_INFO
#
#HOST
"localhost"
#VERSION
"3.43"
#DUT
"Star_Test 	#AMAC 0 ComID 0 HCC "
#TIME
"12:47:50"
#
%DCS_INFO
#
#T0	T1
. 	. 	
#VDET	IDET
. 	. 	
#VCC	ICC
. 	. 	
#VDD	IDD
. 	. 	
#TIME_POWERED
. 
#
%StrobeDelay
#
#DELAY
#  M00  S01  S02  S03  S04  S05  S06  S07  S08  E09
15 18 17 17 18 18 16 15 20 17 -1 -1 -1 -1 -1 -1 
#
#  M00  S01  S02  S03  S04  S05  S06  S07  S08  E09
15 18 17 17 18 18 16 15 20 17 -1 -1 -1 -1 -1 -1 
#
#No defects found!
%Comment
COMMENT        : Strobe Delay Fraction 0.57
#
#
%NewTest
#
SERIAL NUMBER  : BNL-PP2-MLS-005
TEST MADE BY   : qq
LOCATION NAME  : BNL_4MODULEBOX
Run number     : 620-3
TEST_DATE      : 27/12/2019
PASSED         : NO
PROBLEM        : NO
#
%DAQ_INFO
#
#HOST
"localhost"
#VERSION
"3.43"
#DUT
"Star_Test 	#AMAC 0 ComID 0 HCC "
#TIME
"12:55:00"
#
%DCS_INFO
#
#T0	T1
. 	. 	
#VDET	IDET
. 	. 	
#VCC	ICC
. 	. 	
#VDD	IDD
. 	. 	
#TIME_POWERED
. 
#
#
%SCAN_INFO
#
#POINT_TYPE
"QCAL (fC)"
#N_POINTS
3
#POINTS
0.50 	1.00 	1.50 	
#
#
%ThreePointGain
#
#Loop A - Fit
#	func	p0	p1	p2
#M00
	4 	16.30 	-22.76 	0.00
#S01
	4 	16.88 	-24.35 	0.00
#S02
	4 	16.39 	-23.02 	0.00
#S03
	4 	16.71 	-23.87 	0.00
#S04
	4 	16.46 	-23.20 	0.00
#S05
	4 	16.15 	-22.37 	0.00
#S06
	4 	16.19 	-22.47 	0.00
#S07
	4 	16.29 	-22.74 	0.00
#S08
	4 	16.61 	-23.60 	0.00
#E09
	4 	16.16 	-22.41 	0.00
#M00
	4 	16.29 	-22.74 	0.00
#S01
	4 	16.84 	-24.24 	0.00
#S02
	4 	16.39 	-23.01 	0.00
#S03
	4 	16.70 	-23.86 	0.00
#S04
	4 	64.36 	95.42 	0.00
#S05
	4 	16.12 	-22.29 	0.00
#S06
	4 	16.17 	-22.43 	0.00
#S07
	4 	16.28 	-22.71 	0.00
#S08
	4 	16.57 	-23.51 	0.00
#E09
	4 	16.17 	-22.44 	0.00
#
#Loop B - Gain, Offset, Noise at 1.00fC
#	vt50 	rms  	gain	rms 	offset	rms  	outnse	innse	rms
#M00
	###    Too many defects in this chip!    ###
#S01
	###    Too many defects in this chip!    ###
#S02
	###    Too many defects in this chip!    ###
#S03
	###    Too many defects in this chip!    ###
#S04
	###    Too many defects in this chip!    ###
#S05
	###    Too many defects in this chip!    ###
#S06
	###    Too many defects in this chip!    ###
#S07
	###    Too many defects in this chip!    ###
#S08
	###    Too many defects in this chip!    ###
#E09
	###    Too many defects in this chip!    ###
#M00
	###    Too many defects in this chip!    ###
#S01
	###    Too many defects in this chip!    ###
#S02
	###    Too many defects in this chip!    ###
#S03
	###    Too many defects in this chip!    ###
#S04
	###    Too many defects in this chip!    ###
#S05
	###    Too many defects in this chip!    ###
#S06
	###    Too many defects in this chip!    ###
#S07
	###    Too many defects in this chip!    ###
#S08
	###    Too many defects in this chip!    ###
#E09
	###    Too many defects in this chip!    ###
#
#Loop C - Comment
#  M00  S01  S02  S03  S04  S05  S06  S07  S08  E09
"low gain" 	"low gain" 	"low gain" 	"low gain" 	"low gain" 	"low gain" 	"low gain" 	"low gain" 	"low gain" 	"low gain" 	
#  M00  S01  S02  S03  S04  S05  S06  S07  S08  E09
"low gain" 	"low gain" 	"low gain" 	"low gain" 	"unbonded" 	"low gain" 	"low gain" 	"low gain" 	"low gain" 	"low gain" 	
#BadChannelSummary - not for the database
#  at 1.00fC
#		lost	dodgy	dead	stuck	ineff	unbon	lo_gn	hi_gn	lo_off	hi_off	partbon	hi_nse
#Chip M00:	  0	128	  0	  0	128	  0	  0	  0	  0	  0	  0	  0
#Chip S01:	  0	128	  0	  0	128	  0	  0	  0	  0	  0	  0	  0
#Chip S02:	  0	128	  0	  0	128	  0	  0	  0	  0	  0	  0	  0
#Chip S03:	  0	128	  0	  0	128	  0	  0	  0	  0	  0	  0	  0
#Chip S04:	  0	128	  0	  0	128	  0	  0	  0	  0	  0	  0	  0
#Chip S05:	  0	128	  0	  0	128	  0	  0	  0	  0	  0	  0	  0
#Chip S06:	  0	128	  0	  0	128	  0	  0	  0	  0	  0	  0	  0
#Chip S07:	  0	128	  0	  0	128	  0	  0	  0	  0	  0	  0	  0
#Chip S08:	  0	128	  0	  0	128	  0	  0	  0	  0	  0	  0	  0
#Chip E09:	  0	128	  0	  0	128	  0	  0	  0	  0	  0	  0	  0
#Chip M00:	  2	128	  0	  0	128	  2	  0	  0	  0	  0	  0	  0
#Chip S01:	  2	128	  0	  0	128	  2	  0	  0	  0	  0	  0	  0
#Chip S02:	  2	128	  0	  0	128	  2	  0	  0	  0	  0	  0	  0
#Chip S03:	  2	128	  0	  0	128	  2	  0	  0	  0	  0	  0	  0
#Chip S04:	  2	127	  0	  0	127	  2	  0	  0	  0	  0	  0	  0
#Chip S05:	  2	128	  0	  0	128	  2	  0	  0	  0	  0	  0	  0
#Chip S06:	  2	128	  0	  0	128	  2	  0	  0	  0	  0	  0	  0
#Chip S07:	  4	126	  2	  0	126	  4	  2	  0	  2	  0	  0	  0
#Chip S08:	  2	128	  0	  0	128	  2	  0	  0	  0	  0	  0	  0
#Chip E09:	  2	128	  0	  0	128	  2	  0	  0	  0	  0	  0	  0
#Link 0:	  0	1280	  0	  0	1280	  0	  0	  0	  0	  0	  0	  0
#Link 1:	 22	1277	  2	  0	1277	 22	  2	  0	  2	  0	  0	  0
#Link 2:	 22	2557	  2	  0	2557	 22	  2	  0	  2	  0	  0	  0
%Defect
DEFECT NAME    : UNBONDED
FIRST CHANNEL  : 1328
LAST CHANNEL   : 1328
#
%Defect
DEFECT NAME    : UNBONDED
FIRST CHANNEL  : 1369
LAST CHANNEL   : 1369
#
%Defect
DEFECT NAME    : UNBONDED
FIRST CHANNEL  : 1456
LAST CHANNEL   : 1456
#
%Defect
DEFECT NAME    : UNBONDED
FIRST CHANNEL  : 1497
LAST CHANNEL   : 1497
#
%Defect
DEFECT NAME    : UNBONDED
FIRST CHANNEL  : 1584
LAST CHANNEL   : 1584
#
%Defect
DEFECT NAME    : UNBONDED
FIRST CHANNEL  : 1625
LAST CHANNEL   : 1625
#
%Defect
DEFECT NAME    : UNBONDED
FIRST CHANNEL  : 1712
LAST CHANNEL   : 1712
#
%Defect
DEFECT NAME    : UNBONDED
FIRST CHANNEL  : 1753
LAST CHANNEL   : 1753
#
%Defect
DEFECT NAME    : UNBONDED
FIRST CHANNEL  : 1840
LAST CHANNEL   : 1840
#
%Defect
DEFECT NAME    : UNBONDED
FIRST CHANNEL  : 1968
LAST CHANNEL   : 1968
#
%Defect
DEFECT NAME    : UNBONDED
FIRST CHANNEL  : 2009
LAST CHANNEL   : 2009
#
%Defect
DEFECT NAME    : UNBONDED
FIRST CHANNEL  : 2096
LAST CHANNEL   : 2096
#
%Defect
DEFECT NAME    : UNBONDED
FIRST CHANNEL  : 2137
LAST CHANNEL   : 2137
#
%Defect
DEFECT NAME    : UNBONDED
FIRST CHANNEL  : 2224
LAST CHANNEL   : 2224
#
%Defect
DEFECT NAME    : DEAD
FIRST CHANNEL  : 2251
LAST CHANNEL   : 2251
#
%Defect
DEFECT NAME    : UNBONDED
FIRST CHANNEL  : 2265
LAST CHANNEL   : 2265
#
%Defect
DEFECT NAME    : DEAD
FIRST CHANNEL  : 2277
LAST CHANNEL   : 2277
#
%Defect
DEFECT NAME    : UNBONDED
FIRST CHANNEL  : 2352
LAST CHANNEL   : 2352
#
%Defect
DEFECT NAME    : UNBONDED
FIRST CHANNEL  : 2393
LAST CHANNEL   : 2393
#
%Defect
DEFECT NAME    : UNBONDED
FIRST CHANNEL  : 2480
LAST CHANNEL   : 2480
#
%Defect
DEFECT NAME    : UNBONDED
FIRST CHANNEL  : 2521
LAST CHANNEL   : 2521
#
#21 defects found affecting 2559 strips
#1280 maximum consecutive defects
%Comment
COMMENT        : SELECT = 0
#
%Comment
COMMENT        : FAIL due to chip M0 low gain
#
%Comment
COMMENT        : FAIL due to chip S1 low gain
#
%Comment
COMMENT        : FAIL due to chip S2 low gain
#
%Comment
COMMENT        : FAIL due to chip S3 low gain
#
%Comment
COMMENT        : FAIL due to chip S4 low gain
#
%Comment
COMMENT        : FAIL due to chip E5 low gain
#
%Comment
COMMENT        : FAIL due to chip S6 low gain
#
%Comment
COMMENT        : FAIL due to chip S7 low gain
#
%Comment
COMMENT        : FAIL due to chip S8 low gain
#
%Comment
COMMENT        : FAIL due to chip S9 low gain
#
%Comment
COMMENT        : FAIL due to chip M8 low gain
#
%Comment
COMMENT        : FAIL due to chip S9 low gain
#
%Comment
COMMENT        : FAIL due to chip S10 low gain
#
%Comment
COMMENT        : FAIL due to chip S11 low gain
#
%Comment
COMMENT        : FAIL due to chip E13 low gain
#
%Comment
COMMENT        : FAIL due to chip S14 low gain
#
%Comment
COMMENT        : FAIL due to chip S15 low gain
#
%Comment
COMMENT        : FAIL due to chip S16 low gain
#
%Comment
COMMENT        : FAIL due to chip S17 low gain
#
%Comment
COMMENT        : FAIL due to 2559 bad strips (more than 15)
#
%Comment
COMMENT        : FAIL due to 1280 consecutive bad strips (more than 8)
#
#
%TEST Rawdata
FILENAME       : /home/qcbox/Desktop/stars/itsdaq-sw/DATA/results/BNL-PP2-MLS-005_RC_620_3.txt
#
